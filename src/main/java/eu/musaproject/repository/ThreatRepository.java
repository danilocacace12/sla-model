package eu.musaproject.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import eu.musaproject.model.ComponentType;
import eu.musaproject.model.QuestionThreat;
import eu.musaproject.model.Threat;

public interface ThreatRepository extends JpaRepository<Threat, Long> {
	
	Threat findByThreatId(String threatId);
	
	@Query(value = "SELECT * FROM threats_strides", nativeQuery = true )
	List<String> getThreatStrides();
	
	List<Threat> findByComponentTypes(Set<ComponentType> componentType);
	
	List<Threat> findByQuestions(Set<QuestionThreat> questionThreat);
}
