package eu.musaproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.musaproject.model.QuestionThreat;

public interface QuestionThreatRepository extends JpaRepository<QuestionThreat, Long> {

	QuestionThreat findByQuestionId(String questionId);
}
