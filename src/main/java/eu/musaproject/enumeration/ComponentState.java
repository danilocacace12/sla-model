package eu.musaproject.enumeration;

public enum ComponentState {
	CREATED,
	PROFILED,
	SLAT_GENERATED,
	SLAT_ASSESSED,
	SLA_GENERATED
}
