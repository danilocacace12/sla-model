package eu.musaproject.enumeration;

public enum ApplicationState {
	ERROR,
	CREATED,
	PROFILED,
	SLATs_GENERATED,
	SLAs_GENERATED,
	SLATs_ASSESSED,
	ASSESSED_SLAs_GENERATED
}
