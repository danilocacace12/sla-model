package eu.musaproject.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "component_control_questions")
public class ComponentControlQuestion {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "component_control_id")
	private ComponentControl componentControl;
	
	@OneToOne
	@JoinColumn(name = "question_control_id")
	private QuestionControl questionControl;
	
	@Column(name = "answer")
	private String answer;
	

	public ComponentControlQuestion(){}
	
	public ComponentControlQuestion(QuestionControl questionControl, ComponentControl componentControl){
		setComponentControl(componentControl);
		setQuestionControl(questionControl);
	}

	public ComponentControl getComponentControl() {
		return componentControl;
	}

	public void setComponentControl(ComponentControl componentControl) {
		this.componentControl = componentControl;
	}

	public QuestionControl getQuestionControl() {
		return questionControl;
	}

	public void setQuestionControl(QuestionControl questionControl) {
		this.questionControl = questionControl;
	}	

	public Long getId() {
		return id;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}
	
}
