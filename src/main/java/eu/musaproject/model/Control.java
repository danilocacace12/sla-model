package eu.musaproject.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "controls")
public class Control {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "control_id", unique=true)
	private String controlId;

	@Column(name = "control_name")
	private String controlName;

	@Column(name = "family_id")
	private String familyId;

	@Column(name = "family_name")
	private String familyName;

	@Column(name = "control")
	private Integer control;

	@Column(name = "enhancement")
	private Integer enhancement;

	@Column(name = "control_description")
	@Type(type="text")
	private String controlDescription;
	
	@ManyToMany(fetch = FetchType.EAGER)
	private Set<Metric> metrics;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Set<QuestionControl> questionsControl;

	@Column(name = "risk")
	private String risk;

	private Boolean selected;

	public Control(){
		metrics = new HashSet<Metric>();
		questionsControl = new HashSet<QuestionControl>();
	}

	public String getControlId() {
		return controlId;
	}

	public void setControlId(String controlId) {
		this.controlId = controlId;
	}

	public String getControlName() {
		return controlName;
	}

	public void setControlName(String controlName) {
		this.controlName = controlName;
	}

	public String getFamilyId() {
		return familyId;
	}

	public void setFamilyId(String familyId) {
		this.familyId = familyId;
	}

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	public Integer getControl() {
		return control;
	}

	public void setControl(Integer control) {
		this.control = control;
	}

	public Integer getEnhancement() {
		return enhancement;
	}

	public void setEnhancement(Integer enhancement) {
		this.enhancement = enhancement;
	}

	public String getControlDescription() {
		return controlDescription;
	}

	public void setControlDescription(String controlDescription) {
		this.controlDescription = controlDescription;
	}

	public Long getId() {
		return id;
	}
	
	public Set<Metric> getMetrics() {
		return metrics;
	}

	public void setMetrics(Set<Metric> metrics) {
		this.metrics = metrics;
	}
	
	public void addMetric(Metric metric) {
		getMetrics().add(metric);
	}
	
	public void resetQuestionControl() {
		questionsControl = new HashSet<QuestionControl>();
	}
	
	public Set<QuestionControl> getQuestionControl() {
		return questionsControl;
	}

	public void setQuestionControl(Set<QuestionControl> questionsControl) {
		this.questionsControl = questionsControl;
	}
	
	public void addQuestionControl(QuestionControl questionControl) {
		getQuestionControl().add(questionControl);
	}
	
	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}
	
	public String getRisk() {
		return risk;
	}

	public void setRisk(String risk) {
		this.risk = risk;
	}

}
