package eu.musaproject.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import eu.musaproject.enumeration.ApplicationState;

@Entity
@Table(name = "applications")
public class Application {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="kanbanId", unique=true)
	private String kanbanId;
	
	@Column(name="macmId", unique=true)
	private String macmId;

	@Column(name="name")
	private String name;
	
	@Column(name="image_url")
	private String imageUrl;
	
	@Column(name="composed_sla_id")
	private String composedSlaId;

	@Enumerated(EnumType.STRING)
	private ApplicationState state;

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "application")
	private Set<Component> components;

	
	public Application(){
		components = new HashSet<>();
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}
	
	public Set<Component> getComponents() {
		return components;
	}

	public void addComponent(Component component) {
		this.getComponents().add(component);
		if(component.getApplication() != this){
			component.setApplication(this);
		}
	}
	
	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	public ApplicationState getState() {
		return state;
	}

	public void setState(ApplicationState state) {
		this.state = state;
	}
	
	public String getKanbanId() {
		return kanbanId;
	}

	public void setKanbanId(String kanbanId) {
		this.kanbanId = kanbanId;
	}
	
	public String getMacmId() {
		return macmId;
	}

	public void setMacmId(String macmId) {
		this.macmId = macmId;
	}
	
	public String getComposedSlaId() {
		return composedSlaId;
	}

	public void setComposedSlaId(String composedSlaId) {
		this.composedSlaId = composedSlaId;
	}
	
}
