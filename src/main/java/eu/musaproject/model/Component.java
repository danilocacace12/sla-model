package eu.musaproject.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnore;

import eu.musaproject.enumeration.ComponentState;

@Entity
@Table(name = "components")
public class Component {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="kanbanId")
	private String kanbanId;
	
	@ManyToOne
	@JsonIgnore
	private Application application;
	
	@Column(name="name")
	private String name;
	
	@Column(name="description")
	private String description;

	@Column(name="slatSync")
	private Boolean slatSync;

	@Enumerated(EnumType.STRING)
	private ComponentState state;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_component_type")
	private ComponentType componentType;

	@Column(name="component_group")
	private String componentGroup;

	@Column(name="risk_profile")
	@Type(type="text")
	private String riskProfile;

	@OneToMany(mappedBy = "component", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<ComponentThreat> componentThreats;
	
	@OneToMany(mappedBy = "component", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<ComponentControl> componentControls;
	
	@Column(name="slat_id")
	private String slatId;
	
	@Column(name="assessed_slat_id")
	private String assessedSlatId;
	
	@Column(name="composed_sla_id")
	private String composedSlaId;

	public Component(){
		componentThreats = new HashSet<ComponentThreat>();
		componentControls = new HashSet<ComponentControl>();
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getId() {
		return id;
	}
	
	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}
	
	public Set<ComponentThreat> getComponentThreats() {
		return componentThreats;
	}

	public void addComponentThreat(ComponentThreat componentThreat) {
		getComponentThreats().add(componentThreat);
		if(componentThreat.getComponent() != this){
			componentThreat.setComponent(this);
		}
	}
	
	public void removeAllComponentThreats() {
		getComponentThreats().clear();
	}
	
	public Set<ComponentControl> getComponentControls() {
		return componentControls;
	}

	public void addComponentControl(ComponentControl componentControl) {
		getComponentControls().add(componentControl);
		if(componentControl.getComponent() != this){
			componentControl.setComponent(this);
		}
	}
	
	public void removeAllComponentControls() {
		getComponentControls().clear();
	}

	
	public String getRiskProfile() {
		return riskProfile;
	}

	public void setRiskProfile(String riskProfile) {
		this.riskProfile = riskProfile;
	}
	
	public ComponentState getState() {
		return state;
	}

	public void setState(ComponentState state) {
		this.state = state;
	}
	
	public String getKanbanId() {
		return kanbanId;
	}

	public void setKanbanId(String kanbanId) {
		this.kanbanId = kanbanId;
	}
	
	public String getSlaId() {
		return slatId;
	}

	public void setSlaId(String slaId) {
		this.slatId = slaId;
	}
	
	public String getAssessedSlaId() {
		return assessedSlatId;
	}

	public void setAssessedSlaId(String assessedSlaId) {
		this.assessedSlatId = assessedSlaId;
	}
	
	public String getComosedSlaId() {
		return composedSlaId;
	}

	public void setComosedSlaId(String composedSlaId) {
		this.composedSlaId = composedSlaId;
	}
	
	public Boolean getSlatSync() {
		return slatSync;
	}

	public void setSlatSync(Boolean slatSync) {
		this.slatSync = slatSync;
	}
	
	public ComponentType getComponentType() {
		return componentType;
	}

	public void setComponentType(ComponentType componentType) {
		this.componentType = componentType;
	}
	
	public String getComponentGroup() {
		return componentGroup;
	}

	public void setComponentGroup(String componentGroup) {
		this.componentGroup = componentGroup;
	}
	
}
